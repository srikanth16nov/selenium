package windowhandling;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LearnWindowHandling {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.hyrtutorials.com/p/window-handles-practice.html");
		String parentWindow = driver.getWindowHandle();
		System.out.println("Parent Window: "+parentWindow);
		driver.findElement(By.xpath("//button[contains(text(),'Open New Window')]")).click();
		Set<String> childWindows = driver.getWindowHandles();
		for (String child : childWindows) {
			System.out.println(child);
			if(!parentWindow.equals(child)) {
				driver.switchTo().window(child);
			}
		}
		driver.findElement(By.name("fName")).sendKeys("Srikanth");
		driver.findElement(By.name("lName")).sendKeys("M");
		driver.close();
		driver.switchTo().window(parentWindow);
		driver.findElement(By.id("name")).sendKeys("Sri");

	}

}
