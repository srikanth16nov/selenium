package windowhandling;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LearnWindowHandlingTabs {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.hyrtutorials.com/p/window-handles-practice.html");
		String parentWindow = driver.getWindowHandle();
		System.out.println("Parent Window: "+parentWindow);
		
		driver.findElement(By.id("newTabBtn")).click();
		Set<String> childWindows = driver.getWindowHandles();
		for (String child : childWindows) {
			System.out.println(child);
		}

	}

}
