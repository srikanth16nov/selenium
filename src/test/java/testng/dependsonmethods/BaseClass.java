package testng.dependsonmethods;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import io.github.bonigarcia.wdm.WebDriverManager;


public class BaseClass {
	public static RemoteWebDriver driver;

	@BeforeMethod(alwaysRun = true)
	@Parameters({ "webUrl" ,"browser","uName","uPass" }) 
	public void setUp( String webUrl ,String browser,String uName,String uPass ) {
	
		if(browser.equalsIgnoreCase("Chrome")) {
			//Chrome Driver Setup
			WebDriverManager.chromedriver().setup();

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notifications");
			//Launch Chrome Browser
			driver = new ChromeDriver(options);
		}else if(browser.equalsIgnoreCase("Firefox")) {
			WebDriverManager.firefoxdriver().setup();
			FirefoxOptions options = new FirefoxOptions();
			options.setProfile(new FirefoxProfile());
			options.addPreference("dom.webnotifications.enabled", false);
			driver = new FirefoxDriver(options);
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		//TS-[1-3] - Launch the app
		driver.get(webUrl);

		//Enter the Username
		driver.findElementByXPath("//input[@id='username']").sendKeys(uName);

		//Enter the Password
		driver.findElementById("password").sendKeys(uPass);

		//Click Login Button 
		driver.findElementById("Login").click();
	}
	
	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		driver.close();
	}



}
