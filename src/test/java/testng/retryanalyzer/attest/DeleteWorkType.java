package testng.retryanalyzer.attest;


import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


public class DeleteWorkType extends BaseClass{

	@Test(groups = "sanity",retryAnalyzer = testng.retryanalyzer.attest.RetryAnalyzer.class)
	public void deleteWorkType() {

		// 4) Click on the App Laucher Icon left to Setup
		driver.findElementByClassName("slds-icon-waffle/").click();

		// 5) Click on View All
		driver.findElementByXPath("//button[contains(text(),'View All')]").click();

		// 6) Click on Work Types
		WebElement workTypeWebElement = driver.findElementByXPath("//p[contains(text(),'Work Types')]");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", workTypeWebElement);

		// 7) Click on the Arrow button at the end of the first result
		driver.findElementByXPath("//tbody/tr/td[5]").click();

		// 8) Click on Delete
		driver.findElementByXPath("//a[@title='Delete']").click();

		// 9) Click on Delete
		driver.findElementByXPath("//h2[text()='Delete Work Type']/../..//span[text()='Delete']").click();

		// 10) Verify the message
		String textExp = "was deleted";
		String textAct = driver.findElementByXPath("//button[@title='Close']/preceding-sibling::div//child::span").getText();
		System.out.println("Actual Delete Text is: "+textAct);
		if(textAct.contains(textExp)) {
			System.out.println("Delete text is Verified");
		}
	}

}
