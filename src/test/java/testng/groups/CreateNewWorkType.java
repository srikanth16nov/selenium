package testng.groups;

import java.io.IOException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utility.ReadExcelData;


public class CreateNewWorkType extends BaseClass{
	String dataFileName;
	//public static RemoteWebDriver driver;

	@BeforeClass(groups="smoke")
	public void dataSource() {
		dataFileName="CreateNewWorkType";
	}

	@DataProvider(name="workTypeData")
	public String[][] testData() throws IOException {

		ReadExcelData readEx = new ReadExcelData();
		String[][] exData = readEx.readDataFromExcel(dataFileName);
		return exData;

	}
	
	
	
	@Test (dataProvider = "workTypeData",groups="smoke")
	public void createNewWorkType(String workTypeName,String description,String newOperatingHours,String estimateDuration) {

		//TS-4 Click on toggle menu button from the left corner
		driver.findElementByClassName("slds-icon-waffle").click();

		//TS-5 Click view All		
		driver.findElementByXPath("//button[contains(text(),'View All')]").click();

		//TS-6 Click on Work Types
		WebElement workTypeWebElement = driver.findElementByXPath("//p[contains(text(),'Work Types')]");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", workTypeWebElement);

		//TS-7 Click on New
		driver.findElementByXPath("//div[contains(text(),'New')]").click();

		//TS-8 Enter Work Type Name as 'Salesforce Project'
		driver.findElementByXPath("//span[contains(text(),'Work Type Name')]//parent::label//following-sibling::input").sendKeys(workTypeName);

		//TS-9 Enter Description as 'Specimen'
		driver.findElementByTagName("textarea").sendKeys(description);

		//TS-10 Create new operating hours by Entering a name as 'UK Shift'
		WebElement opHourBoxClick = driver.findElementByXPath("//input[@title='Search Operating Hours']");
		js.executeScript("arguments[0].click();", opHourBoxClick);

		
		driver.findElementByXPath("//span[contains(text(),'New Operating Hours')]").click();;
		driver.findElementByXPath("//span[starts-with(text(),'Name')]//parent::label//following-sibling::input").sendKeys(newOperatingHours);

		//TS-11 Select '(GMT+00:00) Greenwich Mean Time (GMT)' for Time Zone
		driver.findElementByXPath("//div[@class='uiPopupTrigger']//descendant::a[contains(text(),'Pacific')]").click();


		js.executeScript("arguments[0].click();", driver.findElementByXPath("//a[@title='(GMT+00:00) Greenwich Mean Time (GMT)']"));

		//TS-12 Click on Save
		driver.findElementByXPath("//div[@class='modal-footer slds-modal__footer']//child::div//child::button[@title='Save']").click();

		//TS-13 Enter Estimated Duration as '7'
		driver.findElementByXPath("//span[starts-with(text(),'Estimated Duration')]//parent::label//following-sibling::input").sendKeys(estimateDuration);

		//TS-14 Click on Save
		driver.findElementByXPath("//button[@title='Save']/span[starts-with(text(),'Save')]").click();

		//TS-15 Verify the Created message 
		String newWorkTypeDesAct ="Specimen";
		String newWorkTypeDesExp = driver.findElementByXPath("//span[starts-with(text(),'Specimen')]").getText();
		if(newWorkTypeDesAct.contentEquals(newWorkTypeDesExp)) {
			System.out.println("New Work Type Create Validation Completed");
		}


	}
	
}


