package testng.groups;

import java.io.IOException;



import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utility.ReadExcelData;


public class EditWorkType extends BaseClass {
	String dataFileName;

@BeforeClass(groups = "sanity")
public void dataSource() {
	dataFileName="EditWorkType";
}
	
@DataProvider(name="editTypeData")
public String[][] testData() throws IOException{
	ReadExcelData readEx = new ReadExcelData();
	String[][] excelData = readEx.readDataFromExcel(dataFileName);
	return excelData;
}

	@Test(dataProvider = "editTypeData", groups = "sanity")
public void editWorkType(String timeFrameStart,String timeFrameEnd) {
		
		//4) Click on the App Laucher Icon left to Setup
		driver.findElementByClassName("slds-icon-waffle").click();

		//5) Click on View All
		driver.findElementByXPath("//button[contains(text(),'View All')]").click();

		//6) Click on Work Types

		WebElement workTypeWebElement = driver.findElementByXPath("//p[contains(text(),'Work Types')]");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", workTypeWebElement);

		// 7) Click on the Arrow button at the end of the first result

		driver.findElementByXPath("//tbody/tr/td[5]").click();

		// 8) Click on Edit

		driver.findElementByXPath("//a[@title='Edit']").click();

		// 9) Enter Time Frame Start as '9'
		driver.findElementByXPath("//span[text()='Timeframe Start']/../following-sibling::input").sendKeys(timeFrameStart);
		
		// 10) Enter Time Frame End as '18'
		driver.findElementByXPath("//span[text()='Timeframe End']/../following-sibling::input").sendKeys(timeFrameEnd);
		
		// 11) Click on Save
		driver.findElementByXPath("//div[@data-aura-class='forceRecordEditActions']//descendant::button[@title='Save']").click();
		
		// 12) Verify the success message
		String textExp = "was saved";
		String textAct = driver.findElementByXPath("//button[@title='Close']/preceding-sibling::div//child::span").getText();
		System.out.println("Actual text is: "+textAct);
		if(textAct.contains(textExp)) {
			System.out.println("Success message Validated");
		}
	}
}

