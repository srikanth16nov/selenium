package codingchallenge30days;

import java.awt.AWTException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;


import io.github.bonigarcia.wdm.WebDriverManager;

public class Day4CodingChallege {

	public static void main(String[] args) throws InterruptedException, AWTException {

		WebDriverManager.edgedriver().setup();

		EdgeOptions option = new EdgeOptions();
		//option.setAcceptInsecureCerts(true);
		EdgeDriver driver = new EdgeDriver(option);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("Browser version: "+driver.getCapabilities().getVersion());
		driver.get("https://www.redbus.in/");

		driver.findElementById("dest").sendKeys("Bangalore");
		Thread.sleep(5000);

		driver.findElementById("dest").sendKeys("+ {TAB}");

		driver.findElementById("src").sendKeys("Chennai");
		Thread.sleep(5000);

		driver.findElementById("src").sendKeys("{TAB}");

		driver.findElementById("onward_cal").click();
		driver.findElementByXPath("//button[text()='>']").click();

		boolean row1MondayStatus = driver.findElementByXPath("//table//tr[3]/td[@class='empty day']").isDisplayed();
		if(row1MondayStatus) {
			driver.findElementByXPath("//table//tr[4]/td[@class='wd day'][1]").click();
		}else {
			driver.findElementByXPath("//table//tr[3]/td[@class='wd day'][1]").click();
		}

		driver.findElementById("search_btn").click();

		Thread.sleep(3000);

		List<WebElement> elementBus = driver.findElementsByXPath("//li[contains(@class,'row-sec')]");
		System.out.println("Total no of Buses: "+elementBus.size());




	}

}
