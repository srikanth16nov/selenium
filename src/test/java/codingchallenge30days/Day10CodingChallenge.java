package codingchallenge30days;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Day10CodingChallenge {
	
	public static int lowerCaseCount(String link) {
		int upperCase=0;
		int lowerCase=0;
		char[] ch = link.toCharArray();
		for (char c : ch) {
			if(c >= 'Z' && c <= 'A') {
				upperCase++;
			}else if(c >= 'z' && c <= 'a'){
				lowerCase++;
			}else {
				continue;
			}
		}
		return lowerCase;
		
	}
	
	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.ajio.com/");
		
		//driver.findElementByXPath("//div[@class='ic-close-quickview']").click();
	
		Actions act = new Actions(driver);
		act.moveToElement(driver.findElementByLinkText("WOMEN")).build().perform();
		//act.moveToElement(driver.findElementByLinkText("BRANDS")).build().perform();
		List<WebElement> eleLinks = driver.findElementsByTagName("a");
		Integer[] lowerCaseCount = new Integer[eleLinks.size()];
		for (int i = 0; i < eleLinks.size(); i++) {
			String text = eleLinks.get(i).getText();
			lowerCaseCount[i] = lowerCaseCount(text);
			System.out.println(text);
		}
		
		for (int i = 0; i < lowerCaseCount.length; i++) {
			System.out.println(lowerCaseCount[i]);
		}
		
		
	}

}
