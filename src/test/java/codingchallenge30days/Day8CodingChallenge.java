package codingchallenge30days;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Day8CodingChallenge {

	public static void main(String[] args) {
		
		WebDriverManager.firefoxdriver().setup();
		FirefoxDriver driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		driver.get("https://www.flipkart.com/");
		
		driver.findElementByXPath("//button[text()='✕']").click();
		
		WebElement eleSearch = driver.findElementByXPath("//input[@title='Search for products, brands and more']");
		eleSearch.sendKeys("Home Theaters");
		eleSearch.sendKeys(Keys.ENTER);
		
		List<WebElement> eleProductRating = driver.findElementsByXPath("//span[contains(@id,'productRating')]/div");
		List<Double> ratingList = new ArrayList<Double>();
		
		for (int i = 0; i < eleProductRating.size(); i++) {
			ratingList.add(Double.parseDouble(eleProductRating.get(i).getText()));
		}
		int count=0;
		for(int i=0;i<ratingList.size();i++) {
			if(ratingList.get(i)>=4) {
				System.out.println(ratingList.get(i));
				count++;
			}
		}
		System.out.println("Count above 4* : "+count);
		
	
	

}}
