package codingchallenge30days;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import io.github.bonigarcia.wdm.WebDriverManager;

public class Day16CodingChallenge {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.bankbazaar.com/personal-loan.html");
		
		driver.findElementByXPath("//span[contains(@class,'iconSalaried')]").click();
		
		driver.findElementByXPath("//input[@placeholder='Start typing here....']").sendKeys("INFOSYS");
		driver.findElementByXPath("(//div[@class='Select-menu']/div)[2]").click();
		
		Thread.sleep(5000);
		Actions act = new Actions(driver);
		act.dragAndDropBy(driver.findElementByXPath("//div[@class='rangeslider__handle']"), 302, 0).build().perform();
		driver.findElementByLinkText("Continue").click();
		
		driver.findElementByXPath("//input[@placeholder='PIN Code']").sendKeys("600001");
		driver.findElementByLinkText("Continue").click();
		

		driver.findElementByName("mobileNumber").sendKeys("842001515");
		driver.findElementByLinkText("Submit").click();
		
		String errorMessage = driver.findElementByXPath("//span[@class='errorMessage']").getText();
		System.out.println("Warning Message is: "+errorMessage);
		
		driver.close();
		
		
	}

}
