package codingchallenge30days;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Day12CodingChallenge {

	public static void main(String[] args) throws IOException {
		
		WebDriverManager.chromedriver().setup();
		RemoteWebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		driver.get("https://www.espncricinfo.com");
		driver.switchTo().frame("google_ads_iframe_21783347309/espn.cricinfo.com/frontpage/index_2");
		//if(driver.findElementByXPath("//a[@class='sprite close']").isDisplayed()) {
			js.executeScript("arguments[0].click();", driver.findElementByXPath("//a[@class='sprite close']"));
		//}
			driver.switchTo().defaultContent();
		try {
			js.executeScript("arguments[0].click();", driver.findElementByXPath("(//div[contains(@class,'sidebar-widget')])[1]//descendant::span[text()='IPL 2020']"));
		}catch(NoSuchElementException e) {
			js.executeScript("arguments[0].click();", driver.findElementByXPath("(//span[text()='IPL 2020']/..)[1]"));
		}
		TakesScreenshot tkscr = (TakesScreenshot)driver;
		File screenshotAs = tkscr.getScreenshotAs(OutputType.FILE);
		File dstFile = new File("./snapshot/1.jpg");
		FileUtils.copyFile(screenshotAs, dstFile);

	}

}
