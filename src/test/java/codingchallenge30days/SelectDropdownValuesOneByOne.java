package codingchallenge30days;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.tools.ant.taskdefs.WaitFor.Unit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SelectDropdownValuesOneByOne {

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.facebook.com/");
		driver.findElementByXPath("//a[text()='Create New Account']").click();
		WebElement eleDay = driver.findElementByXPath("//select[@id='day']");
		System.out.println("Facebook");
		Select sel = new Select(eleDay);
		List<WebElement> listDays = driver.findElementsByXPath("//select[@id='day']/option");
		for (int i = 0; i < listDays.size(); i++) {
			sel.selectByVisibleText(listDays.get(i).getText());
			Thread.sleep(1000);
			//sel.deselectByVisibleText(listDays.get(i).getText());
		}

	}

}
