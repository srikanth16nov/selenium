package codingchallenge30days;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Day18CodingChallenge {

	public static void main(String[] args) throws AWTException, InterruptedException, IOException  {
		
		WebDriverManager.chromedriver().setup();
		//Headless mode getting NoSuchElement error and it's working fine normal mode 
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors");
		
		ChromeDriver driver = new ChromeDriver(options);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//driver.manage().window().maximize();
		driver.get("https://www.zomato.com/");
		TakesScreenshot tkscr = (TakesScreenshot)driver;
		File screenshotAs = tkscr.getScreenshotAs(OutputType.FILE);
		File dstFile = new File("./snapshot/2.jpg");
		FileUtils.copyFile(screenshotAs, dstFile);
		WebDriverWait wait = WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElementByXPath("//input[@placeholder='Search for restaurant, cuisine or a dish']")));
				 
		driver.findElementByXPath("//input[@placeholder='Search for restaurant, cuisine or a dish']").click();
		Robot rbt = new Robot();
		rbt.keyPress(KeyEvent.VK_A);
		rbt.keyPress(KeyEvent.VK_2);
		rbt.keyPress(KeyEvent.VK_B);
		Thread.sleep(5000);
		List<WebElement> noOfRest = driver.findElementsByXPath("//p[contains(text(),'A2B')]");
		driver.findElementByXPath("(//p[contains(text(),'A2B')])["+noOfRest.size()+"]").click();
		
		js.executeScript("arguments[0].click();", driver.findElementByLinkText("Order Online"));
		System.out.println("****************************************");
		String text = driver.findElementByXPath("((//h1[text()='A2B - Adyar Ananda Bhavan']/following-sibling::section)[2]/section/span)[1]").getText();
		System.out.println(text);
		System.out.println("****************************************");
		int mustTryCount = driver.findElementsByXPath("//div[text()='MUST TRY']").size();
		System.out.println("Number of Must Try Item is: "+mustTryCount);
		
		List<WebElement> swtElements = driver.findElementsByXPath("//h4[text()='Sweets']/../descendant::span[starts-with(text(),'₹')]");
		Set<Double> highPrice = new HashSet<Double>();
		for (WebElement each : swtElements) {
			highPrice.add(Double.parseDouble((each.getText().replaceAll("₹", ""))));
		}
		System.out.println("****************************************");
		
		List<Double> lsPrice = new ArrayList<Double>();
		lsPrice.addAll(highPrice);
		Double maxPrice = Collections.max(lsPrice);
		String maxFinalPrice="";
		String doubleAsString = String.valueOf(maxPrice);
		int indexOfDecimal = doubleAsString.indexOf(".");
		if(doubleAsString.substring(indexOfDecimal).equals(".0")) {
			maxFinalPrice=doubleAsString.substring(0, indexOfDecimal);
		}else {
			maxFinalPrice=doubleAsString;
		}
		System.out.println("Max Sweet price is: "+maxFinalPrice);
		System.out.println("****************************************");
		System.out.println("Sweet Names are: ");
		List<WebElement> elementsByXPath = driver.findElementsByXPath("//h4[text()='Sweets']/../descendant::span[contains(text(),"+maxFinalPrice+")]");
		for (int i = 1; i <= elementsByXPath.size(); i++) {
			System.out.println(driver.findElementByXPath("(//h4[text()='Sweets']/../descendant::span[contains(text(),"+maxFinalPrice+")])["+i+"]/../preceding-sibling::h4").getText());
		}
		System.out.println("****************************************");
		driver.findElementByLinkText("Photos").click();
		
		String noOfImages = driver.findElementByXPath("//div[contains(text(),'Showing')]").getText();
		String[] split = noOfImages.split("of");
		
		System.out.println("All the pages having: "+split[1]);
		
		driver.close();
	}

	private static WebDriverWait WebDriverWait(ChromeDriver driver, int i) {
		// TODO Auto-generated method stub
		return null;
	}

}
