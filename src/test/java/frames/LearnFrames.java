package frames;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LearnFrames {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://letcode.in/frame");
		driver.switchTo().frame(0);
		driver.findElement(By.name("fname")).sendKeys("Srikanth");
		//driver.findElement(By.name("lname")).sendKeys("M");
		WebElement lnameElement = driver.findElement(By.name("lname"));
		lnameElement.sendKeys("M");
		driver.switchTo().frame(0);
		driver.findElement(By.name("email")).sendKeys("srikanth@gmail.com");
		driver.switchTo().parentFrame();
		lnameElement.clear();
		lnameElement.sendKeys("Murugavel");
		//driver.findElement(By.name("lname")).clear();
		//driver.findElement(By.name("lname")).sendKeys("Murugavel");
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath("//button[contains(text(),'Refer the video')]")).click();
		
	}

}
