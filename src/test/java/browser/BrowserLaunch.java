package browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BrowserLaunch {

	public static void main(String[] args) {
		
		// Using Java System->class and setProperty->method chrome driver exe file assigned
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		
		// WebDriver-> Interface used with ChromeDriver ObjectRefernce, scope restriction applied here. Only WebDriver methods applicable in this implementation
		// WebDriver Interface is Starting point for Selenium and it's extended by SearchContext Interface
		// WebDriver Implementation class are RemoteWebDriver and EventFiringWebDriver
		WebDriver wdriver = new ChromeDriver();

		// RemoteWebDriver is Super class all the browser drivers 
		// RemoteWebDriver implements the WebDriver and JavascriptExecutor interfaces 
		RemoteWebDriver rdriver = new ChromeDriver();

		ChromeDriver driver = new ChromeDriver();

		wdriver.navigate().to("http://leaftaps.com/opentaps/control/main");
		
		rdriver.get("https://www.google.com/");
		
		driver.navigate().to("https://www.linkedin.com/");
		

	}

}
