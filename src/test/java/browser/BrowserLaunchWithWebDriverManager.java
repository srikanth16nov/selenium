package browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BrowserLaunchWithWebDriverManager {
	
	public static void main(String[] args) {
		// WebDriverManager is not provided by Selenium, it's developed by 3rd party 
		// WebDriverManager takecare of WebDriver property setup and no need to download the browser driver 
		// FirefoxDriver setup through WebDriverManager
		WebDriverManager.firefoxdriver().setup();
		//ChromeDriver setup through WebDriver Manager
		WebDriverManager.chromedriver().setup();
		
		WebDriver fdriver = new FirefoxDriver();
		WebDriver cdriver = new ChromeDriver();
		
		fdriver.get("http://leaftaps.com/opentaps/control/main");
		
	}

}
