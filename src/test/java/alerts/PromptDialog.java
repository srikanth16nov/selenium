package alerts;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class PromptDialog {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.leafground.com/pages/Alert.html");
		driver.findElement(By.xpath("//button[text()='Prompt Box']")).click();
		
		Alert alert = driver.switchTo().alert();
		//read alert text
		System.out.println(alert.getText());
		//enter the value		
		alert.sendKeys("Srikanth");
		Thread.sleep(5000);
		//accept alert
		//alert.accept();
		//decline the alert
		alert.dismiss();
		
		

	}

}
