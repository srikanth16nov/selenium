package alerts;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ConfirmationDialog {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.leafground.com/pages/Alert.html");
		driver.findElement(By.xpath("//button[text()='Confirm Box']")).click();
		Alert alert = driver.switchTo().alert();
		System.out.println(alert.getText());
		//UnhandledAlertException
		//driver.findElement(By.xpath("//button[text()='Prompt Box']")).click();
		//alert.accept();
		alert.dismiss();
		String alertButtonText = driver.findElement(By.xpath("//p[@id='result']")).getText();
		System.out.println(alertButtonText);
	}

}
