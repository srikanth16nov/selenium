package utility;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelData {
	
	public String[][] readDataFromExcel(String fileName) throws IOException {

		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+fileName+".xlsx");
		XSSFSheet wsheet = wbook.getSheet("Sheet1");
		int rowCount = wsheet.getLastRowNum();
		int cellNum = wsheet.getRow(1).getLastCellNum();
		
		String[][] exData = new String[rowCount][cellNum];
		for(int i=1;i<=rowCount;i++) {
			for (int j=0;j<cellNum;j++) {
				
				exData[i-1][j] = wsheet.getRow(i).getCell(j).getStringCellValue();

			}
		}
		wbook.close();
		return exData;

	}

}
