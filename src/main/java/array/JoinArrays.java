package array;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

public class JoinArrays {

	public static void main(String[] args) {
		
		int[] array1 = {1,2,3};
		int[] array2 = {4,5,6};

		int[] array3 = ArrayUtils.addAll(array1, array2);
		
		System.out.println("********* Join Arrays Using Apache Commons Method ************");
		System.out.println(Arrays.toString(array3));
		
	}

}
